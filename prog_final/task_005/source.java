import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;

import static java.lang.Math.min;
import static java.lang.Math.sqrt;

public class Main {
    // Collisions
    private FastScanner in;
    private PrintWriter out;

    class Point {
        double x, y;

        Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    private double eps = 1e-7;

    // distance between points a and b
    private double dist(Point a, Point b) {
        return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }

    // the point where the robot would be if
    // covered the distance s from point a in the direction of point b
    private Point goTo(Point a, Point b, double s) {
        double S = dist(a, b);
        return new Point(a.x + (b.x - a.x) / S * s, a.y + (b.y - a.y) / S * s);
    }

    // robot pathways and radii
    private int n, m; // number of robots and number of points in the pathways of each robot
    private double[] radius, speed; // robot radii and speeds
    private Point[][] p; // all points of all pathways
    private double[][] time;

    // initialization
    private void init() throws IOException {
        n = in.nextInt();
        m = in.nextInt();

        radius = new double[n];
        speed = new double[n];
        p = new Point[n][m];
        time = new double[n][m];

        for (int i = 0; i < n; i++) {
            radius[i] = in.nextInt();
            speed[i] = in.nextInt();

            for (int j = 0; j < m; j++)
                p[i][j] = new Point(in.nextInt(), in.nextInt());
        }
    }

    // fill the table of time
    private void fillTime() {
        for (int i = 0; i < n; i++) {
            time[i][0] = 0.0;

            for (int j = 0; j + 1 < m; j++)
                time[i][j + 1] = time[i][j] + dist(p[i][j], p[i][j + 1]) / speed[i];
        }
    }

    // basis of the solution
    private void solve() throws IOException {
        StringBuilder ans = new StringBuilder();
        int cnt = 0;

        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                if (crash(i, j)) {
                    ans.append(i + 1).append(' ').append(j + 1).append('\n');
                    cnt++;
                }

        out.print(cnt + "\n" + ans);
    }

    // Check on collisions robot i and j
    private boolean crash(int i, int j) {
        double pt = 0.0, nt;
        double l, r, t;
        double tl, tr, dl, dr;
        Point pil, pir, pjl, pjr;

        for (int ii = 1, jj = 1; ii < m && jj < m; pt = nt) {
            nt = min(time[i][ii], time[j][jj]);

            if (nt - pt > eps) {

                // ternary search
                l = pt;
                r = nt;
                while (r - l > eps) {

                    tl = l + (r - l) / 3;
                    tr = tl + (r - l) / 3;

                    pil = goTo(p[i][ii - 1], p[i][ii], (tl - time[i][ii - 1]) * speed[i]);
                    pir = goTo(p[i][ii - 1], p[i][ii], (tr - time[i][ii - 1]) * speed[i]);

                    pjl = goTo(p[j][jj - 1], p[j][jj], (tl - time[j][jj - 1]) * speed[j]);
                    pjr = goTo(p[j][jj - 1], p[j][jj], (tr - time[j][jj - 1]) * speed[j]);

                    dl = dist(pil, pjl);
                    dr = dist(pir, pjr);

                    if (dl > dr)
                        l = tl;
                    else
                        r = tr;
                }

                t = (l + r) / 2.0;
                pil = goTo(p[i][ii - 1], p[i][ii], (t - time[i][ii - 1]) * speed[i]);
                pjl = goTo(p[j][jj - 1], p[j][jj], (t - time[j][jj - 1]) * speed[j]);

                if (dist(pil, pjl) - radius[i] - radius[j] < eps)
                    return true;
            }
            
            if (time[i][ii] - nt < eps)
                ii++;
            if (time[j][jj] - nt < eps)
                jj++;
        }

        return false;
    }

    class FastScanner {
        StringTokenizer st;
        BufferedReader br;

        FastScanner(InputStream s) {
            br = new BufferedReader(new InputStreamReader(s));
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreTokens())
                st = new StringTokenizer(br.readLine());
            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }
    }

    private void run() throws IOException {
        in = new FastScanner(System.in);
        out = new PrintWriter(System.out);

        init();
        fillTime();
        solve();

        out.flush();
        out.close();
    }

    public static void main(String[] args) throws IOException {
        new Main().run();
    }
}

