import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static java.lang.Math.min;
import static java.lang.Math.sqrt;

public class Main {
    // Complete cleaning
    private FastScanner in;
    private PrintWriter out;

    class Point {
        double x, y;

        Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    private double eps = 1e-7;

    // distance between points a and b
    private double dist(Point a, Point b) {
        return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }

    // distance between point с and segment ab
    private double dist(Point a, Point b, Point c) {
        double A = a.y - b.y, B = b.x - a.x, C = b.y * a.x - a.y * b.x;
        double cc = B * c.x - A * c.y, d = A * A + B * B;
        Point p = new Point((cc * B - C * A) / d, -(cc * A + C * B) / d);

        return dist(a, p) + dist(p, b) - dist(a, b) < eps ? 
                   dist(c, p) : 
                   min(dist(c, a), dist(c, b));
    }

    // the point where would be robot, if
    // covered the distance s from point a in the direction of point b
    private Point goTo(Point a, Point b, double s) {
        double S = dist(a, b);
        return new Point(a.x + (b.x - a.x) / S * s, a.y + (b.y - a.y) / S * s);
    }

    // pathways and radii of robots
    private int n, m; // number of robots and the number of points in the pathways of each robot
    private double[] radius, speed; // sppeds and radii of robots
    private Point[][] p; // all points of all pathways
    private double[][] time;

    // garbage
    private int k;
    private Point[] pk;

    // initialization
    private void init() throws IOException {
        n = in.nextInt();
        m = in.nextInt();
        k = in.nextInt();

        radius = new double[n];
        speed = new double[n];
        p = new Point[n][m];
        pk = new Point[k];

        for (int i = 0; i < n; i++) {
            radius[i] = in.nextInt();
            speed[i] = in.nextInt();

            for (int j = 0; j < m; j++)
                p[i][j] = new Point(in.nextInt(), in.nextInt());
        }

        for (int i = 0; i < k; i++)
            pk[i] = new Point(in.nextInt(), in.nextInt());
    }

    // fill the table of time
    private void fillTime() {
        time = new double[n][m];
        
        for (int i = 0; i < n; i++) {
            time[i][0] = 0.0;

            for (int j = 0; j + 1 < m; j++)
                time[i][j + 1] = time[i][j] + dist(p[i][j], p[i][j + 1]) / speed[i];
        }
    }

    // Basis of solution
    private void solve() throws IOException {
        
        // create collision matrix
        boolean[][] crash = new boolean[n][n];
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                crash[i][j] = crash[j][i] = crash(i, j);
                
        // create a matrix to check the capture of garbage by robots
        boolean[][] can = new boolean[n][k];
        for (int i = 0; i < n; i++)
            for (int ki = 0; ki < k; ki++)
                for (int j = 0; j + 1 < m && !can[i][ki]; j++)
                    can[i][ki] = dist(p[i][j], p[i][j + 1], pk[ki]) - radius[i] < eps;

        int full = 1 << n, ans = n;
        boolean ok;
        ArrayList<Integer> ids = new ArrayList<>();

        int max = 0, min = 0;
        // full iteration of (bitmasks) options for robot launch
        for (int f = 1; f < full; f++) {
            ids.clear();
            
            // create a list of robots that launch
            for (int i = 0; i < n; i++)
                if (((1 << i) & f) > 0)
                    ids.add(i);
            
            // check robots from the list for collisions
            ok = true;
            for (int i : ids)
                for (int j : ids)
                    ok &= !crash[i][j];
                    
            // in the event of a collision, immediately proceed to the next option
            if (!ok)
                continue;
                
            // count the number of garbage points
            int cnt = 0;
            for (int ki = 0; ki < k; ki++) {
                ok = false;
                for (int i : ids)
                    if (can[i][ki]) {
                        ok = true;
                        break;
                    }
                    
                if (ok)
                    cnt++;
            }
            
            // Improve the current answer
            if (cnt > max) {
                max = cnt;
                min = ids.size();
            } else if (cnt == max)
                if (ids.size() < min)
                    min = ids.size();
        }
        
        // Output the result: max – number of garbage, min – number of launched robots
        out.println(max + " " + min);
    }

    // check on collisions robot i and j
    private boolean crash(int i, int j) {
        double pt = 0.0, nt;
        double l, r, t;
        double tl, tr, dl, dr;
        Point pil, pir, pjl, pjr;

        for (int ii = 1, jj = 1; ii < m && jj < m; pt = nt) {
            nt = min(time[i][ii], time[j][jj]);

            if (nt - pt > eps) {

                // ternary search
                l = pt;
                r = nt;
                while (r - l > eps) {

                    tl = l + (r - l) / 3;
                    tr = tl + (r - l) / 3;

                    pil = goTo(p[i][ii - 1], p[i][ii], (tl - time[i][ii - 1]) * speed[i]);
                    pir = goTo(p[i][ii - 1], p[i][ii], (tr - time[i][ii - 1]) * speed[i]);

                    pjl = goTo(p[j][jj - 1], p[j][jj], (tl - time[j][jj - 1]) * speed[j]);
                    pjr = goTo(p[j][jj - 1], p[j][jj], (tr - time[j][jj - 1]) * speed[j]);

                    dl = dist(pil, pjl);
                    dr = dist(pir, pjr);

                    if (dl > dr)
                        l = tl;
                    else
                        r = tr;
                }

                t = (l + r) / 2.0;
                pil = goTo(p[i][ii - 1], p[i][ii], (t - time[i][ii - 1]) * speed[i]);
                pjl = goTo(p[j][jj - 1], p[j][jj], (t - time[j][jj - 1]) * speed[j]);

                if (dist(pil, pjl) - radius[i] - radius[j] < eps)
                    return true;
            }
            if (time[i][ii] - nt < eps)
                ii++;
            if (time[j][jj] - nt < eps)
                jj++;
        }

        return false;
    }

    class FastScanner {
        StringTokenizer st;
        BufferedReader br;

        FastScanner(InputStream s) {
            br = new BufferedReader(new InputStreamReader(s));
        }

        String next() throws IOException {
            while (st == null || !st.hasMoreTokens())
                st = new StringTokenizer(br.readLine());
            return st.nextToken();
        }

        int nextInt() throws IOException {
            return Integer.parseInt(next());
        }
    }

    private void run() throws IOException {
        in = new FastScanner(System.in);
        out = new PrintWriter(System.out);

        init();
        fillTime();
        solve();

        out.flush();
        out.close();
    }

    public static void main(String[] args) throws IOException {
        new Main().run();
    }
}