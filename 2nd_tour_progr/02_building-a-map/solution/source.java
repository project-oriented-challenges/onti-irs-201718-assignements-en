
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static double life_cycle;
    private static double time;
    private static double naScolko;
    private static double minS;

    public static void main(String[] args) throws IOException {
	
        Scanner scan = new Scanner(System.in);
		
        life_cycle = Double.valueOf(scan.next().replace(',','.'));
        time = Double.valueOf(scan.next().replace(',','.'));
        naScolko = Double.valueOf(scan.next().replace(',','.'));
        minS = Double.valueOf(scan.next().replace(',','.'));

        //create arrays of coordinates and input data
        ArrayList<Cordinates> arrayOfCoordinates = new ArrayList<>();
        ArrayList<Nums> arrayOfInput = new ArrayList<>();
        ArrayList<Double> arrayOfDalnomer = new ArrayList<>();
        ArrayList<Double> arrayOf_L = new ArrayList<>();
        ArrayList<CoordinatesOfPolygon> arrayPointsPolygon = 
        new ArrayList<>();
        ArrayList<Double> array = new ArrayList<>();
        ArrayList<Double> arrayCor = new ArrayList<>();

        //fill the array of input data
        int counter = 0;
        arrayOfInput.add(new Nums(0, 0, 0));
        while (counter != life_cycle) {
            arrayOfInput.add(new Nums(Double.valueOf(scan.next().
                replace(',', '.')), Double.valueOf(scan.next().replace(',', '.')),
            Double.valueOf(scan.next().replace(',', '.'))));
            counter++;
        }

        //add motion coordinates to an array of coordinates
        arrayOfCoordinates.add(new Cordinates(0, 0));
        double angle = 0;

        for (int i = 1; i < arrayOfInput.size(); i++) {

            angle = angle + arrayOfInput.get(i).angleSpeed * time;

            arrayOfCoordinates.add(new Cordinates(arrayOfCoordinates.get(i - 1).x +
                arrayOfInput.get(i).lineSpeed * time * Math.cos(angle),
                arrayOfCoordinates.get(i - 1).y + arrayOfInput.get(i).lineSpeed * 
                time * Math.sin(angle)));
        }
		//display coordinates y
        for (int i = 0; i < arrayOfCoordinates.size(); i++) {
            arrayCor.add(i, arrayOfCoordinates.get(i).getY() * (-1));
        }

        angle = 0;
        for (int i = 0; i < arrayOfInput.size(); i++) {
            angle = angle + arrayOfInput.get(i).angleSpeed * time;
            array.add(angle);
        }

        //fill the array of visible distances and if there is 100(we can not see anything) set -1
        for (int i = 0; i < arrayOfInput.size(); i++) {
            if (arrayOfInput.get(i).dalnomer != 100) {
                arrayOfDalnomer.add(arrayOfInput.get(i).dalnomer);
            } else if (arrayOfInput.get(i).dalnomer == 100) {
                arrayOfDalnomer.add(-1.0);
            }
        }
        
        //receiving and filling the array with the coordinates of the game field that we go round
        //coordinates of the game field
        double xP;
        double yP;

        for (int i = 1; i < arrayOfDalnomer.size(); i++) {

            if (arrayOfDalnomer.get(i) != -1.0) {

                xP = arrayOfCoordinates.get(i).getX() + naScolko * 
                    Math.cos(array.get(i)) + Math.sin(array.get(i)) * 
                    arrayOfDalnomer.get(i) * 10;
               
                yP = arrayCor.get(i) - naScolko * Math.sin(array.get(i)) + 
                    Math.cos(array.get(i)) * arrayOfDalnomer.get(i) * 10;

                arrayPointsPolygon.add(new CoordinatesOfPolygon(xP, yP));

            }

        }

		//use the Gauss formula to calculate the area
        double s = 0, s1 = 0;
        for (int j = 0; j < arrayPointsPolygon.size() - 1; j++) {
            s = s + (arrayPointsPolygon.get(j).getX() * 
                arrayPointsPolygon.get(j + 1).getY());
            s1 = s1 + (arrayPointsPolygon.get(j).getY() * 
                arrayPointsPolygon.get(j + 1).getX());
        }
        s = s + arrayPointsPolygon.get(arrayPointsPolygon.size() - 1).getX() * 
            arrayPointsPolygon.get(0).y;
        s1 = s1 + arrayPointsPolygon.get(arrayPointsPolygon.size() - 1).getY() * 
            arrayPointsPolygon.get(0).x;

        DecimalFormat form = new DecimalFormat("#.##");
        form.setRoundingMode(RoundingMode.HALF_DOWN);
        form.setMinimumFractionDigits(2);
        double space = (s1 - s) / 2;
      
		//sign test of area
        if (space < 0) {
            space = space * (-1);
            if (space >= minS) {
                System.out.println("True");
            } else if (space < minS) {
                System.out.println("False");
            }
        } else if (space > 0) {
            if (space >= minS) {
                System.out.println("True");
            } else if (space < minS) {
                System.out.println("False");
            }
        }
    }
}
class CoordinatesOfPolygon {
    double x;
    double y;

    public CoordinatesOfPolygon(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
class Cordinates {
    double x;
    double y;


    public Cordinates(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}
class Dalnomer {
    double dalnomer;

    public void Dalnomer(double dalnomer) {
        this.dalnomer = dalnomer;
    }

    public double getDalnomer() {
        return dalnomer;
    }
}
class Nums {
    double lineSpeed;
    double angleSpeed;
    double dalnomer;

    public Nums(double lineSpeed, double angleSpeed, double dalnomer) {
        this.angleSpeed = angleSpeed;
        this.lineSpeed = lineSpeed;
        this.dalnomer = dalnomer;
    }

    public double getLineSpeed() {
        return lineSpeed;
    }

    public double getAngleSpeed() {
        return angleSpeed;
    }

    public double getDalnomer() {
        return dalnomer;
    }
}

class Smen {
    double hy;
    double lx;

    public Smen(double hy, double lx) {
        this.hy = hy;
        this.lx = lx;
    }

    public double getHy() {
        return hy;
    }

    public double getLx() {
        return lx;
    }
}
