used = [];
v = [];

def dfs(x):
    used[x] = True
    for i in range(len(v[x])):
        to = int(v[x][i])
        if used[to] == False:
            dfs(to)

n = int(input())

v = [[] for i in range(n)]
used = [False] * n

for i in range(n) :
    p = input().split();
    for j in range(len(p)) :
        p[j] = int(p[j]) - 1
    for j in range(1, len(p)):
        v[p[j]].append(i);

k = int(input())

if k > 0:
    edit = input().split()

    for i in range(k):
        edit[i] = int(edit[i]) - 1

    for i in range(k):
        dfs(edit[i])

ans = 0;

for i in range(n):
    if used[i] == True:
        ans = ans + 1;

print(ans);
