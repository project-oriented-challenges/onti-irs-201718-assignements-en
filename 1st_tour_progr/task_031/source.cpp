#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	vector <int> lines(4);
	for (int i = 0; i < 4; i++) {
		cin >> lines[i];
	}
	sort(lines.begin(), lines.end());
	if (lines[0] + lines[1] + lines[2] > lines[3]) {
		cout << "Yes\n";
	} else {
		cout << "No\n";
	}
}