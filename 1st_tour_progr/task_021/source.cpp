#include <iostream>
#include <string>

using namespace std;

bool is_dig(char c) {
	if ('0' <= c && c <= '9') {
		return true;
	}
	return false;
}

bool is_let(char c) {
	if ('A' <= c && c <= 'Z') {
		return true;
	}
	return false;
}

int main() {
	string s;
	cin >> s;
	if (is_let(s[0]) && is_dig(s[1]) && is_dig(s[2])
	   && is_dig(s[3]) && is_let(s[4]) && is_let(s[5])) {
		cout << "Yes\n";
	} else {
		cout << "No\n";
	}
}