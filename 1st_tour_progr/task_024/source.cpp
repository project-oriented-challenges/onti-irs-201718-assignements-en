#include <iostream>
#include <vector>

using namespace std;

int const MAX = (int)1e5;

bool used[MAX];

void dfs(int v, vector < vector <int> > &vv) {
	used[v] = true;
	for (auto i : vv[v]) {
		if (used[i]) continue;
		dfs(i, vv);
	}
}	

int main() {
	int n, m, a, b;
	cin >> n >> m >> a >> b;
	--a; --b;
	vector < vector <int> > vv(n);
	for (int i = 0; i < m; i++) {
		int f, t;
		cin >> f >> t;
		--f; --t;
		vv[f].push_back(t);
		vv[t].push_back(f);
	}	
	int k;
	cin >> k;
	for (int i = 0; i < k; i++) {
		int x;
		cin >> x;
		used[x - 1] = true;
	}
	dfs(a, vv);	
	used[b] ? cout << "YES\n" : cout << "NO\n";
	return 0;
}	